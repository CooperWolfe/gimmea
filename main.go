package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"text/tabwriter"
	"text/template"

	"github.com/Masterminds/sprig/v3"
	"github.com/google/uuid"
)

type TemplateEntry struct {
	Name      string
	Directory string
}

func getRoot() string {
	root := os.Getenv("GIMME_ROOT")
	if home := os.Getenv("HOME"); home != "" && root == "" {
		root = strings.TrimRight(home, "/") + "/.gimme"
	}
	_, err := os.Stat(root)
	if os.IsNotExist(err) {
		err = os.Mkdir(root, 0755)
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
	return root
}
func getTemplatesFileName() string {
	root := getRoot()
	return root + "/templates.txt"
}

func validateDirectory(directory string) (string, bool) {
	if directory == "" {
		os.Stderr.WriteString("Directory not provided\n")
		return "", false
	}
	path, err := filepath.Abs(directory)
	dirInfo, err := os.Stat(path)
	if os.IsNotExist(err) || !dirInfo.IsDir() {
		os.Stderr.WriteString("Directory does not exist\n")
		return "", false
	}
	return path, true
}
func validateName(name string) bool {
	if name == "" {
		os.Stderr.WriteString("Name not provided\n")
		return false
	} else if len(name) > 64 {
		os.Stderr.WriteString("Invalid name, max length is 64\n")
		return false
	} else if matched, _ := regexp.MatchString("[a-z0-9-]+", name); !matched {
		os.Stderr.WriteString("Invalid name " + name + ", lowercase alphanumerics and '-' only\n")
		return false
	} else {
		return true
	}
}

func parseTemplatesFile(templatesFile *os.File) []TemplateEntry {
	scanner := bufio.NewScanner(templatesFile)
	var numEntries uint32
	for scanner.Scan() {
		numEntries++
	}

	templatesFile.Seek(0, 0)
	scanner = bufio.NewScanner(templatesFile)
	templateEntries := make([]TemplateEntry, numEntries)
	for i := range templateEntries {
		scanner.Scan()
		line := scanner.Text()
		parts := strings.Split(line, "=")
		entryName, entryDir := parts[0], parts[1]
		templateEntries[i] = TemplateEntry{entryName, entryDir}
	}
	return templateEntries
}

func checkAlreadyInstalled(name string, fullDirName string) (bool, []TemplateEntry) {
	templatesFile, err := os.Open(getTemplatesFileName())
	if err != nil && !os.IsNotExist(err) {
		panic(err)
	} else if templatesFile == nil || os.IsNotExist(err) {
		return false, []TemplateEntry{{name, fullDirName}}
	}

	defer templatesFile.Close()
	templateEntries := parseTemplatesFile(templatesFile)
	for i, entry := range templateEntries {
		if entry.Name != name {
			continue
		}
		if entry.Directory == fullDirName {
			os.Stdout.WriteString("Already installed.\n")
			return true, templateEntries
		} else {
			for true {
				os.Stdout.WriteString("This name has already been assigned to another template directory. Would you like to overwrite it (y/n)? ")
				var yOrN string
				fmt.Scanln(&yOrN)
				switch strings.ToLower(yOrN) {
				case "y", "yes":
					templateEntries[i].Directory = fullDirName
					return false, templateEntries
				case "n", "no":
					return true, templateEntries
				}
			}
		}
	}
	templateEntries = append(templateEntries, TemplateEntry{name, fullDirName})
	return false, templateEntries
}

func checkEntryExists(name string) (bool, string) {
	templatesFile, err := os.Open(getTemplatesFileName())
	if err != nil {
		if os.IsNotExist(err) {
			os.Stderr.WriteString("No such template\n")
			return false, ""
		} else {
			panic(err)
		}
	}
	defer templatesFile.Close()

	templateEntries := parseTemplatesFile(templatesFile)
	var templateEntry *TemplateEntry
	for _, entry := range templateEntries {
		if entry.Name == name {
			templateEntry = &entry
			break
		}
	}
	if templateEntry == nil {
		os.Stderr.WriteString("No such template\n")
		return false, ""
	}

	dirInfo, err := os.Stat(templateEntry.Directory)
	if os.IsNotExist(err) {
		os.Stderr.WriteString("Template directory no longer exists\n")
		return false, ""
	} else if err != nil {
		panic(err)
	} else if !dirInfo.IsDir() {
		os.Stderr.WriteString("Template directory no longer exists\n")
		return false, ""
	}
	return true, templateEntry.Directory
}

func writeTemplateEntries(templateEntries []TemplateEntry) {
	templatesFile, err := os.Create(getTemplatesFileName())
	if err != nil {
		panic(err)
	}
	defer templatesFile.Close()

	for _, entry := range templateEntries {
		templatesFile.WriteString(fmt.Sprintf("%s=%s\n", entry.Name, entry.Directory))
	}
}

func install() {
	directory, name := flag.Arg(1), flag.Arg(2)
	fullDirName, isValid := validateDirectory(directory)
	isValid = isValid && validateName(name)
	if !isValid {
		os.Stderr.WriteString("Usage: gimme install <directory> <name>\n")
		return
	}

	isAlreadyInstalled, templateEntries := checkAlreadyInstalled(name, fullDirName)
	if isAlreadyInstalled {
		return
	}

	writeTemplateEntries(templateEntries)
}

func validateOutputDirectory(outputDirectory string) bool {
	if outputDirectory == "" {
		os.Stderr.WriteString("Directory not provided\n")
		return false
	}
	return true
}

func parseTemplateFieldNames(templateDirectory string) []string {
	templateFieldNamesFileName := strings.TrimRight(templateDirectory, "/") + "/gimme.txt"
	templateFieldNamesFile, err := os.Open(templateFieldNamesFileName)
	if os.IsNotExist(err) {
		return make([]string, 0)
	} else if err != nil {
		panic(err)
	}
	defer templateFieldNamesFile.Close()

	var numFieldNames uint8
	scanner := bufio.NewScanner(templateFieldNamesFile)
	for scanner.Scan() {
		numFieldNames++
	}
	fieldNames := make([]string, numFieldNames)
	templateFieldNamesFile.Seek(0, 0)
	scanner = bufio.NewScanner(templateFieldNamesFile)
	for i := range fieldNames {
		scanner.Scan()
		fieldNames[i] = scanner.Text()
	}
	return fieldNames
}

func getFuncMap() template.FuncMap {
	customFuncs := template.FuncMap{
		"lowerCamelCase": func(snakeCase string) string {
			matches := regexp.MustCompile("(_[^_])")
			return matches.ReplaceAllStringFunc(snakeCase, func(cap string) string {
				return strings.ToUpper(strings.TrimLeft(cap, "_"))
			})
		},
		"upperCamelCase": func(snakeCase string) string {
			matches := regexp.MustCompile("((^|_)[^_])")
			return matches.ReplaceAllStringFunc(snakeCase, func(cap string) string {
				return strings.ToUpper(strings.TrimLeft(cap, "_"))
			})
		},
		"lowerKebabCase": func(snakeCase string) string {
			return strings.ReplaceAll(snakeCase, "_", "-")
		},
		"upperKebabCase": func(snakeCase string) string {
			matches := regexp.MustCompile("((^|_)[^_])")
			return matches.ReplaceAllStringFunc(snakeCase, func(cap string) string {
				return strings.ToUpper(strings.ReplaceAll(cap, "_", "-"))
			})
		},
		"upperSnakeCase": func(snakeCase string) string {
			matches := regexp.MustCompile("((^|_)[^_])")
			return matches.ReplaceAllStringFunc(snakeCase, func(cap string) string {
				return strings.ToUpper(cap)
			})
		},
		"humanReadable": func(snakeCase string) string {
			return strings.ReplaceAll(snakeCase, "_", " ")
		},
		"toBool": func(str string) bool {
			switch strings.ToLower(str) {
			case "y", "yes", "t", "true":
				return true
			case "n", "no", "f", "false":
				return false
			default:
				panic(str + " could not be interpreted into a boolean")
			}
		},
	}
	sprigFuncs := sprig.GenericFuncMap()
	allFuncs := make(map[string]interface{}, len(customFuncs)+len(sprigFuncs))
	for key, value := range customFuncs {
		allFuncs[key] = value
	}
	for key, value := range sprigFuncs {
		allFuncs[key] = value
	}
	return allFuncs
}
func createTemplate() *template.Template {
	return template.New(uuid.New().String()).Funcs(getFuncMap())
}
func createFileTemplate(fileName string) *template.Template {
	template, err := template.New(filepath.Base(fileName)).
		Funcs(getFuncMap()).
		ParseFiles(fileName)
	if err != nil {
		os.Stderr.WriteString("Template error in " + fileName + "\n")
		panic(err)
	}
	return template
}

func parseString(str string, fields *map[string]string) string {
	template := createTemplate()
	template, err := template.Parse(str)
	if err != nil {
		panic(err)
	}
	var buffer bytes.Buffer
	err = template.Execute(&buffer, fields)
	if err != nil {
		panic(err)
	}
	return buffer.String()
}

func populateFile(outputFileName string, templateFileName string, fields *map[string]string) {
	outputFile, err := os.Create(outputFileName)
	if err != nil {
		panic(err)
	}
	defer outputFile.Close()

	template := createFileTemplate(templateFileName)
	template.Execute(outputFile, fields)
}

func populateDir(outputDirName string, templateDirName string, fields *map[string]string) {
	templateDir, err := os.Open(templateDirName)
	if err != nil {
		panic(err)
	}
	files, err := templateDir.ReadDir(0)
	if err != nil {
		panic(err)
	}
	for _, f := range files {
		fileName := parseString(f.Name(), fields)
		if fileName == "" {
			continue
		}
		outputFilePath := strings.TrimRight(outputDirName, "/") + "/" + fileName
		templateFilePath := strings.TrimRight(templateDirName, "/") + "/" + f.Name()
		if f.IsDir() {
			_, err = os.Stat(outputFilePath)
			if os.IsNotExist(err) {
				err = os.Mkdir(outputFilePath, 0755)
				if err != nil {
					panic(err)
				}
			} else if err != nil {
				panic(err)
			}
			populateDir(outputFilePath, templateFilePath, fields)
		} else if f.Name() == "gimme.txt" {
			continue
		} else {
			populateFile(outputFilePath, templateFilePath, fields)
		}
	}
}

func new() {
	usage := func() {
		os.Stderr.WriteString("Usage: gimme new <template name> <output directory>\n")
	}
	name, outputDirectory := flag.Arg(1), flag.Arg(2)
	isValid := validateName(name)
	isValid = isValid && validateOutputDirectory(outputDirectory)
	if !isValid {
		usage()
		return
	}
	entryDoesExist, templateDirectory := checkEntryExists(name)
	if !entryDoesExist {
		return
	}

	outputPath, err := filepath.Abs(outputDirectory)
	if err != nil {
		panic(err)
	}
	_, err = os.Stat(outputPath)
	if os.IsNotExist(err) {
		os.Mkdir(outputPath, 0755)
	} else if err != nil {
		panic(err)
	}

	os.Stdout.WriteString("Giving you a " + name + "\n")
	templateFieldNames := parseTemplateFieldNames(templateDirectory)
	templateFields := make(map[string]string, len(templateFieldNames))
	if len(templateFieldNames) > 0 {
		os.Stdout.WriteString("* Remember to prefer lower_snake_case when entering field values\n")
	}
	for _, fieldName := range templateFieldNames {
		os.Stdout.WriteString("   Please provide a value for " + fieldName + ": ")
		var fieldValue string
		fmt.Scanln(&fieldValue)
		templateFields[fieldName] = fieldValue
	}

	populateDir(outputPath, templateDirectory, &templateFields)
}

func list() {
	templatesFile, err := os.Open(getTemplatesFileName())
	if os.IsNotExist(err) {
		os.Stdout.WriteString("There are no templates.\n")
		return
	} else if err != nil {
		panic(err)
	}

	templateEntries := parseTemplatesFile(templatesFile)
	sort.Slice(templateEntries, func(i, j int) bool {
		return templateEntries[i].Name < templateEntries[j].Name
	})

	tabWriter := tabwriter.Writer{}
	tabWriter.Init(os.Stdout, 4, 8, 0, '\t', 0)
	defer tabWriter.Flush()
	for _, entry := range templateEntries {
		fmt.Fprintf(&tabWriter, "%s\t%s\n", entry.Name, entry.Directory)
	}
}

func uninstall() {
	name := flag.Arg(1)
	if !validateName(name) {
		os.Stderr.WriteString("Usage: gimme uninstall <template name>\n")
		return
	}

	templatesFile, err := os.Open(getTemplatesFileName())
	if os.IsNotExist(err) {
		os.Stdout.WriteString("There are no templates.\n")
		return
	} else if err != nil {
		panic(err)
	}

	templateEntries := parseTemplatesFile(templatesFile)
	var i uint8
	newTemplateEntries := make([]TemplateEntry, len(templateEntries)-1)
	for _, entry := range templateEntries {
		if entry.Name != name {
			newTemplateEntries[i] = entry
			i++
		}
	}

	writeTemplateEntries(newTemplateEntries)
}

func main() {
	flag.Parse()
	command := flag.Arg(0)
	switch command {
	case "install":
		install()
	case "uninstall":
		uninstall()
	case "list":
		list()
	case "new":
		new()
	default:
		os.Stderr.WriteString("Invalid command\n  Use one of:\n    - install\n    - uninstall\n    - list\n    - new\n")
	}
}
