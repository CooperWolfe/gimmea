# Introducing Gimme

### A lightweight, powerful boilerplate text template engine, useful for any and all languages and projects.

---

- [Description](#description)
- [Example](#example)
- [Usage](#usage)
- [FAQ](#faq)

---

## Description
There are lots of reasons to start building a boilerplate army. Gimme makes it incredibly easy. Simply tell it what directory represents your boilerplate template:
```
gimme install path/to/directory any-template-name
```
Then start spitting the code back out
```
gimme new any-template-name /path/to/output
```

There's no need to re-install after pointing gimme to the template's root directory, so your team will automatically adopt changes as they `git pull`.

---

## Example
Given this environment:
```
$ tree tmpl/
tmpl/
|
|-> gimme.txt
|-> hello/
|   |
|   |-> file1
|   `-> file{{.Name}}
`-> world/
    |
    |-> file1
    `-> file{{ .Name | upperCamelCase }}

$ cat tmpl/gimme.txt
Name

$ cat tmpl/hello/file1
Why {{ .Name | humanReadable }}
```
Execute this:
```
$ gimme install tmpl/ my-template

$ gimme new my-template output/
Giving you a my-template
* Remember to prefer lower_snake_case when entering field values
   Please provide a value for Name: hello_world
```
To get this:
```
$ tree output/
output/
|
|-> hello/
|   |
|   |-> file1
|   `-> filehello_world
`-> world/
    |
    |-> file1
    `-> fileHelloWorld

$ cat output/file1
Why hello world
```

*Cleanup:*
```
$ gimme list
my-template /path/to/tmpl/

$ gimme uninstall my-template
```

---

## Usage
In the template, all folder and file names, and file contents, can use [go template](https://pkg.go.dev/text/template) syntax. There is only one special file that should sit at the root of your template directory, called `gimme.txt`. This file is simply a newline-separated list of the field names that will be used in the template.

Four commands are supported:
- `gimme install <template directory> <template name>`
- `gimme list`
- `gimme uninstall <template name>`
- `gimme new <template name> <output directory>`

Install on unix using `bash install.sh`

---

## FAQ
### Question: What go template pipeline functions are supported?
Any [go template](https://pkg.go.dev/text/template) functions can be used, as well as all functions from the [sprig](http://masterminds.github.io/sprig/) library. In addition, there are several custom functions I found particularly helpful for boilerplate templating:
- lowerCamelCase: converts a lower_snake_case string to lowerCamelCase
- upperCamelCase: converts a snake_case string to UpperCamelCase
- lowerKebabCase: converts a lower_snake_case string to lower-kebab-case
- upperKebabCase: converts a snake_case string to Upper-Kebab-Case
- upperSnakeCase: converts a snake_case string to Upper_Snake_Case
- humanReadable: converts a snake_case string to human readable case
- toBool: ignoring case, converts a string (y|yes|t|true|n|no|f|false) to a boolean

### Question: Will gimme delete my existing files?
Gimme will not delete existing files or folders, BUT if there are files with the same name as the template files, they will be overwritten.

### Question: Where does Gimme store its data?
You can find the very simple `.txt` file that gimme uses to manage your templates under `$HOME/.gimme`. If you want to put it somewhere else, populate the `GIMME_ROOT` environment variable.

### Question: Is it possible to optionally create files or folders?
Yes. If a file or folder's name evaluates to an empty string, it will not be created. Hint: use go template's `{{ if }}` statements with the custom toBool function.