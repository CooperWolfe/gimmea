# Contribution
While you have every right to fork this repository and run with it, I embrace comments, merge requests, and bug reports.

If you like what you see here and think you can do better yourself, do me a favor and fork from here anyway. It simply directs the credit for what little is here back to where it belongs.

Thanks!